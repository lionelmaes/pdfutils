# PDFUtils

Back at work with PDF manipulations in a prepress perspective
This repo is meant to be the successor of this one : https://github.com/docteurem/PDFutils which was a fork of this one : https://github.com/osp/PDFutils.

The goal is still the same, keeping together programs useful to convert / impose / check pdfs for printing.
