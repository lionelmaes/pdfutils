import sys, getopt
from PyPDF2 import PdfFileWriter, PdfFileReader

#change a single pages pdf to double pages
#because changing the layout in metadata to "/TwoPageRight" doesn't work
#we need to actually make the double pages
def makeDoublePagesPdf(inputF, outputF):

    input = PdfFileReader(open(inputF, "rb"))
    output = PdfFileWriter()

    #first we add the first page of input (that should remain single)
    output.addPage(input.getPage(0))

    for i in range(1,input.getNumPages()-2, 2):
        pageLeft = input.getPage(i)
        pageRight = input.getPage(i+1)
        print("Merging page %s with page %s" % (i, i+1))
         # dimensions for offset from left page (adding it to the right)
        offset_x = pageLeft.mediaBox[2]
        offset_y = 0

        # add second page to first one
        pageLeft.mergeTranslatedPage(pageRight, offset_x, offset_y, expand=True)

        output.addPage(pageLeft)


    #then we add the last page
    output.addPage(input.getPage(input.getNumPages()-1))
    #and done.
    outputStream = open(outputF, "wb")
    output.write(outputStream)

def main(argv):
    try:
        opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
    except getopt.GetoptError:
        print('single-2-double-pages.py -i <inputfile> -o <outputfile>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('single-2-double-pages.py -i <inputfile> -o <outputfile>')
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputF = arg
        elif opt in ("-o", "--ofile"):
            outputF = arg
    makeDoublePagesPdf(inputF, outputF)


if __name__ == "__main__":
   main(sys.argv[1:])
