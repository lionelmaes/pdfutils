import sys, getopt
from PyPDF2 import PdfFileWriter, PdfFileReader


def makeBooklet(inputF, outputF):

    input = PdfFileReader(open(inputF, "rb"))
    output = PdfFileWriter()


   
    for i in range(0,input.getNumPages(), 4):
        hasVerso = False
        if i + 1 < input.getNumPages() :
            hasVerso = True
        

        recto = output.add_blank_page(input.getPage(i).mediabox.getWidth(), input.getPage(i).mediabox.getHeight() * 2)
        if hasVerso:
            verso = output.add_blank_page(input.getPage(i).mediabox.getWidth(), input.getPage(i).mediabox.getHeight() * 2)
        
        recto.mergeTranslatedPage(input.getPage(i), 0,  input.getPage(i).mediaBox.getHeight())

        if hasVerso:
            verso.mergeTranslatedPage(input.getPage(i+1), 0,  input.getPage(i).mediaBox.getHeight())
        
        if i + 2 < input.getNumPages() :
            recto.mergeTranslatedPage(input.getPage(i+2), 0, 0)
        
        if i + 3 < input.getNumPages() :
            verso.mergeTranslatedPage(input.getPage(i+3), 0, 0)

    #and done.
    outputStream = open(outputF, "wb")
    output.write(outputStream)

def main(argv):
    try:
        opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
    except getopt.GetoptError:
        print('double-A6-to-A4-booklet.py -i <inputfile> -o <outputfile>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('double-A6-to-A4-booklet.py -i <inputfile> -o <outputfile>')
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputF = arg
        elif opt in ("-o", "--ofile"):
            outputF = arg
    makeBooklet(inputF, outputF)


if __name__ == "__main__":
   main(sys.argv[1:])
