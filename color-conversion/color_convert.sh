#!/bin/bash
#
# USAGE:
# ./color_convert.sh [OPTION] colormodelayer1,colormodelayer2 input_layer1.pdf,input_layer2.pdf output.pdf
# spot colour colormode should look like "spot-COLOR NAME-0 0.64 0.67 0.02"
# where COLOR NAME is the name of the colour and 0 0.64 0.67 0.02 are cmyk values from 0 to 1
# 
# -d    debug mode activated
# -c    force clean of white background generated by chrome
#
# EXAMPLE: ./color_convert.sh -c "spot-PANTONE 877C-0.46 0.34 0.35 0.14",cmyk test/layer1.pdf,test/layer2.pdf test/output.pdf




DEBUG=0
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

function graytospot {
    [ $DEBUG -eq 1 ] && echo "converting : $1 to $2 \n"

    ####################################################################
    #part1: let's put our custom color reference in gray_to_spot.ps
    IFS='-' read -a args <<< "$2"
    IFS=' ' read -a color <<< "${args[2]}"
    unset IFS
    #args[1] = color name, args[2] = cmyk equivalent in 1 line
    #color[0] = C, color[1] = M, color[2] = Y, color[3] = K
    spotcolor="\/spotcolor [\/Separation (${args[1]}) \/DeviceCMYK{dup ${color[0]} mul exch dup ${color[1]} mul exch dup ${color[2]} mul exch ${color[3]} mul}] def"
    [ $DEBUG -eq 1 ] && echo "spotcolor : $spotcolor \n"
    sed -i "s/^\/spotcolor.*$/$spotcolor/g" $DIR/gray_to_spot.ps
    #endpart1

    ######################################################################
    #part2: convert to ps file using pdftops
    pdftops "$1"
    psfile="${1/.pdf/.ps}"

    ######################################################################
    #part3: modify bitmap decode from [0 1] to [1 0] in generated ps
    sed -i -e 's/Decode \[0 1\]/Decode [1 0]/g' "$psfile"

    ######################################################################
    #part4: convert back to pdf using gray_to_spot.ps
    gs -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sOutputFile="$1" $DIR/gray_to_spot.ps "$psfile"

    ######################################################################
    #part5: remove ps file
    [ $DEBUG -eq 0 ] && rm "$psfile"

}


CLEAN=0

while getopts ":cd" o; do
    case "${o}" in
        c)  
            CLEAN=1;;
        d)
            DEBUG=1;;
    esac
done


set -f
IFS=","
layers=(${@:$OPTIND:1})
input_files=(${@:$OPTIND+1:1})
output_file=(${@:$OPTIND+2:1})

#check if we have three arguments for layers, input_files and output
if [ ${#layers[@]} -eq 0 ] || [ ${#input_files[@]} -eq 0 ] || [ "${output_file}" == "" ]; then
    echo "error: missing argument"
    exit 1
fi

#check if we have the same number of layers and input_files
if [ ${#layers[@]} -ne  ${#input_files[@]} ]; then
    echo "error: number of layers doesn't match number of files"
    exit 1
fi

if [ $DEBUG -eq 1 ] && [ $CLEAN -eq 1 ]; then echo "clean pdf background activated"; fi
if [ $DEBUG -eq 1 ]; then echo "layers : "; printf '%s\n' "${layers[@]}"; fi
if [ $DEBUG -eq 1 ]; then echo "files : "; printf '%s\n' "${input_files[@]}"; fi
if [ $DEBUG -eq 1 ]; then echo "output : ${output_file}"; fi


#loop on files and convert them according to layers colormodes
for index in "${!layers[@]}"
do
    echo "${input_files[index]} ${layers[index]}"

    #remove the default white background on each pdf. See bug https://bugs.chromium.org/p/chromium/issues/detail?id=498892
    if [ $CLEAN -eq 1 ]; then
        qpdf -qdf "${input_files[index]}" "${input_files[index]/.pdf/-readable.pdf}"
        #remove white background with sed: find /G3 gs then next line, then find 0 0 793 1122 re and remove
        sed -i "/G3 gs/{n;s/[[:digit:]]\+ [[:digit:]]\+ [[:digit:]]\+ [[:digit:]]\+ re//;}"  "${input_files[index]/.pdf/-readable.pdf}"
        fix-qdf "${input_files[index]/.pdf/-readable.pdf}" > "${input_files[index]/.pdf/-fixed.pdf}"
        
        if [ $DEBUG -eq 0 ]; then
            rm "${input_files[index]/.pdf/-readable.pdf}"
        fi

        input_files[index]="${input_files[index]/.pdf/-fixed.pdf}"
        
    fi

    if [ "${layers[index]}" = "black"  ] || [[ "${layers[index]}" == spot*  ]]; then
        gs \
        -dNOPAUSE \
        -dBATCH \
        -sDEVICE=pdfwrite \
        -sProcessColorModel=DeviceGray \
        -sColorConversionStrategy=Gray \
        -sDefaultCMYKProfile=ps_cmyk.icc \
        -dOverrideICC \
        -sOutputFile="${input_files[index]/.pdf/-tmp.pdf}" "${input_files[index]}"
        if [[ "${layers[index]}" == spot*  ]]; then
            graytospot "${input_files[index]/.pdf/-tmp.pdf}" "${layers[index]}"
        fi
    elif [ "${layers[index]}" = "cmyk" ]; then
        gs \
        -dNOPAUSE \
        -dBATCH \
        -sDEVICE=pdfwrite \
        -dProcessColorModel=/DeviceCMYK \
        -sColorConversionStrategy=CMYK \
        -sDefaultCMYKProfile=ps_cmyk.icc \
        -dOverrideICC \
        -sOutputFile="${input_files[index]/.pdf/-tmp.pdf}" "${input_files[index]}"
    fi

    if [ $DEBUG -eq 0 ] && [ $CLEAN -eq 1 ]; then
        rm "${input_files[index]}"
    fi



done

####IF SINGLE LAYER
if [ ${#layers[@]} = 1 ]; then
    mv "${input_files[0]/.pdf/-tmp.pdf}" "${output_file}"
    exit
fi

echo "${#layers[@]}"

####ELSE OVERLAY
input_file="${input_files[0]/.pdf/-tmp.pdf}"
for(( i=1; i < ${#layers[@]}; i++ ))
do
    pdftk "$input_file" multistamp "${input_files[i]/.pdf/-tmp.pdf}" output "${output_file/.pdf/$i.pdf}"
    if [ $DEBUG -eq 0 ]; then
        rm "$input_file"
        rm "${input_files[i]/.pdf/-tmp.pdf}"
    fi

    input_file="${output_file/.pdf/$i.pdf}"

done

last_file_n=$(expr ${#layers[@]} - 1)

mv "${output_file/.pdf/$last_file_n.pdf}" "$output_file"
echo "done"

exit 0
